#include<stdio.h>

int main()
{
    float c,f;

    printf("Input the temperature in Celsius: \n");
    scanf("%f",&c);

    f = (c*9/5)+32;

    printf("%f Celsius temperature in Fahrenheit is: %.2f\n",c,f);

    return 0;
}
