#include<stdio.h>

int main ()
{
    int x,y,z,sum;
    float avg;

    printf("Input 3 integers: \n");
    scanf("%d %d %d",&x,&y,&z);

    sum=x+y+z;
    avg=(float)sum/3;

    printf("Sum of the integers is: %d\n",sum);
    printf("Average of the integers is: %.2f\n",avg);

    return 0;
}
