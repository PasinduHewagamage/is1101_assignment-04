#include<stdio.h>
int printb(int);

int main()
{
    unsigned int A=10,B=15;
    unsigned int a=0,b=0,c=0,d=0,e=0;

    a=A&B;
    b=A^B;
    c=~A;
    d=A<<3;
    e=B>>3;

    printf("A&B = %d\n",printb(a));
    printf("A^B = %d\n",printb(b));
    printf("~A = %d\n",printb(c));
    printf("A<<3 = %d\n",printb(d));
    printf("B>>3 = %d\n",printb(e));

    return 0;
}

int printb(int n)
{
    if (n==0) return 0;
    else return (n%2)+10*printb(n/2);
}
