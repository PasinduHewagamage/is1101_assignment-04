#include<stdio.h>

int main()
{
    float r,h,volume;

    printf("Input radius: \n");
    scanf("%f",&r);
    printf("Input height: \n");
    scanf("%f",&h);

    volume = 3.14*r*r*h/3;

    printf("Volume of the cone is: %.2f\n",volume);

    return 0;
}
